import React from "react";
import { useNavigate } from 'react-router-dom'

const MainMenu = () => {
    const nav = useNavigate();
    const toPokedex = () => {
        nav('/pokedex');
    }
    // const toWhoz = () => {
    //     nav('/whosthatpkmn');
    // }
    return (
        <div>
            <div className="main-menu-container">
                <div>
                    <h1>PKMN APP</h1>
                </div>
                <button className="main-menu-btn" onClick={toPokedex}>Pokedex</button>
                <button className="main-menu-btn" >Whoz That PKMN?</button>
            </div>
        </div>
    )
}

export default MainMenu;