import React, { useState, useEffect } from "react";
import Navbar from "./Navbar";
import Pokedex from "./Pokedex";
import Searchbar from "./Searchbar";


const PokedexPage = (props) => {
    const [page, setPage] = useState(0);
    const { pokemons, loading, notFound, onSearchHandler, totalPages, fetchPokemons } = props
    useEffect(() => {
        fetchPokemons();
    }, [page]); 
    return (
        <div>

            <Navbar />
            <Searchbar onSearch={onSearchHandler} />
            {
                notFound ? (
                    <div className='not-found-text'>Esse pokemon não existe</div>
                ) :
                    (<Pokedex pokemons={pokemons}
                        loading={loading}
                        page={page}
                        totalPages={totalPages}
                        setPage={setPage} />)
            }
        </div>
    )
}

export default PokedexPage;