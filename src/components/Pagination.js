import React from "react";

const Pagination = (props) => {
    const { page, totalPages, onPrevClick, onNextClick } = props
    return (
        <div className="pagination-container">
            <button onClick={onPrevClick}><div>◀</div></button>
            <div>{page} de {totalPages}</div>
            <button onClick={onNextClick}><div>▶</div></button>
        </div>
    )
}

export default Pagination;
