import React, {useState} from "react";

const Whozthat = () => {
    const [inputName, setInputName] = useState();
    const onChangeHandler = (e) => {
        setInputName(e.target.value)
        if(e.target.value === 0){
            onSearch(undefined)
        }
    }
    
    const onButtonClickHandler = () => {
        onSearch(inputName)
    }
    return (
        <div>
            <div>
                Quem é esse pokemon?
            </div>
            <input placeholder="Digite sua Resposta" onChange={onChangeHandler}></input>
            <button onClick={onButtonClickHandler}>Aplicar</button>
        </div>
    )
}

export default Whozthat;