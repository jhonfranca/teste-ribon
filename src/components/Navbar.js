import React, {useContext} from "react";
import FavoriteContext from "../contexts/favoritesContext";
import { useNavigate } from 'react-router-dom'

const Navbar = () => {
    const nav = useNavigate();
    const toMenu = () => {
        nav('/');
    }
    const {favoritePokemons} = useContext(FavoriteContext)
    return (
        <nav>
            <div>
                <img alt="pokedex" 
                src="./pokedex-3d-logo.png"
                className="navbar-img"
                onClick={toMenu}
                 />
            </div>
            <div>{favoritePokemons.length}❤</div>
        </nav>
    );
};

export default Navbar;