# Projeto Pokedex para Ribbon

# Getting Started 

Para rodar o projeto, simplesmente abra o terminal na pasta do projeto após clonar e rode o comando:

### `npm start`

## Nota

Pessoal da Ribon, peço mil desculpas por não conseguir concluir o projeto como imaginei. Pensei algo muito extenso porém não consegui ter tempo hábil e infelizmente meu conhecimento de backend não é muito vasto.

## O que contém na aplicação

O app consiste em uma Pokedéx que lista os 150 primeiros Pokemons, Em cada card de pokemon temos as informações: Nome, id, Tipo 1, Tipo 2, Peso e Altura. 

Contem também um campo de busca para buscar o pokemon desejado.

Existe também um botão de favorito cujo número de favoritos fica salvo no cabeçalho da aplicação. 

## Considerações finais

Enfim, Muito obrigado pela oportunidade e, peço desculpas por não conseguir cumprir com o objetivo principal que era o joguinho. Me passaram diversas coisas sobre a cabeça, tentei bastante realmente. Como consequência, aprendí bastante da ferramenta React, totalmente diferente do que eu estou acostumado em ver usando Vuetify. 
